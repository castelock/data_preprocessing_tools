from imutils import paths
import os

class FindingLabels:
    def __init__(self, dataset_path, labels, new_path):
        self.dataset_path = dataset_path
        self.labels = labels
        self.new_path = new_path

    # Method to organize the different files in folders according to the labels
    def findLabels(self):
        imagePaths = list(paths.list_images(self.dataset_path))
        for imagePath in imagePaths:
            for label in self.labels:
                path_image = imagePath.split(os.path.sep)
                filename = path_image[-1]                
                if filename.find(label) != -1:
                   match label:                   
                    case "[1, 1, 0, 1, 1]":
                        self.moveFile(label, imagePath, filename, self.new_path)
                    case _:
                        print("Label unknown: ", label)    
                else:
                    print("Label unknown: ", label)  
                    continue

    # Moving file to the new path
    def moveFile(self, label, old_image_path, filename, new_path):
        folder_path = os.path.join(new_path,label)
        if os.path.isdir(folder_path):
            os.rename(old_image_path, os.path.join(folder_path, filename))
        else:
            os.mkdir(os.path.join(new_path, label))
            os.rename(old_image_path, os.path.join(folder_path, filename))


dataset_path = "/home/juanje/Documents/Charts_Recognition/test_lissajou/aggregation_filtered"
labels = ["[1, 1, 0, 1, 1]"]
new_path = "/home/juanje/Documents/Charts_Recognition/test_lissajou"
finding = FindingLabels(dataset_path, labels, new_path)
finding.findLabels()
