import os

def rename_files(root_folder):
    # Recorre todos los archivos y subdirectorios en la carpeta raíz
    for root, dirs, files in os.walk(root_folder):
        for file in files:
            if '[' in file or ']' in file:
                # Crear el nuevo nombre reemplazando los caracteres
                new_file = file.replace('[', '(').replace(']', ')')
                # Ruta completa del archivo viejo y nuevo
                old_file_path = os.path.join(root, file)
                new_file_path = os.path.join(root, new_file)
                # Renombrar el archivo
                os.rename(old_file_path, new_file_path)
                print(f'Renamed: {old_file_path} to {new_file_path}')

# Especifica la ruta de la carpeta donde están las imágenes
root_folder = '/home/juanje/Documents/Charts_Recognition/test11_kaggle'
rename_files(root_folder)
